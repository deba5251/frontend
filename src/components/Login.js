import React from "react";
// import loginpic from "./images/login.svg";
import { NavLink } from "react-router-dom";

const Login = () => {
  return (
    <div>
      <section className="signin">
        <div className="container mt-5">
          <div className="signin-content">
            <div className="signin-image">
              {/* <figure>
                     <img src={loginpic} alt="registation pic"/>
                 </figure> */}
              <NavLink to="./signup" className="signup-image-link">
                craete a account{" "}
              </NavLink>
            </div>
            <div className="signin-form">
              <h2 className="form-title">Login</h2>
              <form className="register-form" id="register-form">
                
                <div className="form-group">
                  <label htmlFor="email">
                    <i class="zmdi zmdi-email material-icons-name"></i>
                  </label>
                  <input
                    type="email"
                    name="email"
                    id="email"
                    autoComplete="off"
                    placeholder="Your Email"
                  />
                </div>
                
               
                <div className="form-group">
                  <label htmlFor="password">
                    <i class="zmdi zmdi-lock material-icons-"></i>
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    autoComplete="off"
                    placeholder="Your password"
                  />
                </div>
                
                <div className="form-group form-button">
                  <input
                    type="submit"
                    name="signin"
                    id="signin"
                    className="form-submit"
                    value="register"
                  />
                </div>
              </form>
              <div></div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Login;
