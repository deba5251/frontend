import React from 'react'
import Navbar from './components/Navbar';
import"bootstrap/dist/css/bootstrap.css";
import "./App.css";
import { Route } from 'react-router-dom';
import About from './components/About';
import Home from './components/Home';
import Contact from './components/Contact';
import Login from './components/Login';
import Signup from './components/Signup';
import Errorpage from './components/Errorpage';

const App = () => {
  return (
    <>
      <Navbar/>
      <switch>
      <Route exact path="/"><Home/></Route>
      <Route path="/about"><About/></Route>
      <Route path="/login"><Login/></Route>
      <Route path="/contact"><Contact/></Route>
      <Route path="/Signup"><Signup/></Route>
      <Route path="/Errorpage"><Errorpage/></Route>
    </switch>
    </>
  )
}
export default App;